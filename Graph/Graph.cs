﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph
{
    // Класс для хранения ребра Graph
  public  class Edge
    {
      public  int src, dest, weight;

      public  Edge(int src, int dest, int weight)
        {
            this.src = src;
            this.dest = dest;
            this.weight = weight;
        }
    }

    // Класс для хранения узлов списка смежности
  public  class Node
    {
        int value, weight;

     public  Node(int value, int weight)
        {
            this.value = value;
            this.weight = weight;
        
        }

      public string toString()
        {
            return this.value + " (" + this.weight + ")";
        }
    }

    // Класс для представления graphического объекта
    class Graph
    {
        // Список списков для представления списка смежности
        List<List<Node>> adjList = new List<List<Node>>();

        // Конструктор для построения Graphа
        public Graph(List<Edge> edges)
        {
            // найти вершину с максимальным номером
            int n = 0;
            foreach (Edge e in edges)
            {
                n = Math.Max(n, Math.Max(e.src, e.dest));
            }

            // выделяем память для списка смежности
            for (int i = 0; i <= n; i++)
            {
                adjList.Add(new List<Node>());
            }

            // добавляем ребра в ориентированный graph
            foreach (Edge e in edges)
            {
                // выделяем новый узел в списке смежности от src до dest
                adjList[e.src].Add(new Node(e.dest, e.weight));

            }
        }

        // Функция для печати представления списка смежности Graph
        public static void printGraph(Graph graph)
        {
            int src = 0;
            int n = graph.adjList.Count();

            while (src < n)
            {
                // вывести текущую вершину и все соседние с ней вершины
            
                    foreach (Node edge in graph.adjList[src])
                {
                    Console.Write(src + " ——> " + edge.toString() + "\t");
                }

                Console.WriteLine();
                src++;
            }
        }

        // Функция для печати матрицы смежности
        public static void Mass(List<Edge> edges)
        {

            int[,] mass = new int[6, 6];
            Console.WriteLine("\nМатрица смежности:");
            foreach (Edge e in edges)
            {
                mass[e.src, e.dest] = 1;
            }

            for (int i = 0; i < 6; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < 6; j++)
                {
                    Console.Write("{0}\t", mass[i, j]);
                }
            }
            Reach(mass); 
        }

        public static void Reach(int[,] mass) { // матрица достижимости

            bool flag;
            int count = 0;
            Console.WriteLine("\n\nМатрица достижимости:");
            for (int k = 0; k < 6; k++)
                for (int i = 0; i < 6; i++)
                    for (int j = 0; j < 6; j++)
                        mass[i,j] = Convert.ToInt32((Convert.ToBoolean(mass[i,j]) || (Convert.ToBoolean(mass[i,k]) && Convert.ToBoolean(mass[k,j]))));

            for (int i = 0; i < 6; i++)
            {
                flag = false;
                Console.WriteLine();
                for (int j = 0; j < 6; j++)
                {
                    if (mass[i,j] == 1) {
                        flag = true;
                    }
                 
                    Console.Write("{0}\t", mass[i, j]);

                }
                count = flag? count+=1 : count;
            }
            if (count == 6)
            {
                Console.WriteLine("\nДанный граф является связным");
            }
            else {
                Console.WriteLine("\nДанный граф не является связным");
            }
        }

           
    }
 }



