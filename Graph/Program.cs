﻿using System;
using System.Collections.Generic;

namespace Graph
{
    class Program
    {
        static void Main(string[] args)
        {

         
            List<Edge> edges = new List<Edge>();
            edges.Add(new Edge(0, 1, 6));
            edges.Add(new Edge(1, 2, 7));
            edges.Add(new Edge(2, 0, 5));
            edges.Add(new Edge(2, 1, 4));
            edges.Add(new Edge(3, 2, 10));
            edges.Add(new Edge(4, 5, 1));
            edges.Add(new Edge(5, 4, 3));

            // построить graph из заданного списка ребер
            Graph graph = new Graph(edges);

            // вывести представление списка смежности Graph
            Graph.printGraph(graph);
           // построить массивы
            Graph.Mass(edges);

        }
    }
}
